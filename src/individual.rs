pub static mut A: f64 = 0.0;
pub static mut B: f64 = 2.0;

pub fn set_a(a: f64) {
    unsafe { A = a; }    
}

pub fn set_b(b: f64) {
    unsafe { B = b; }
}

pub trait Individual<ChildOutput = Self> {
    fn random_init() -> Self;
    fn cross_with(&self, rhs: &Self) -> ChildOutput;
    fn calculate_fitness(&mut self) -> f64;
    fn mutate(&mut self, index: usize);
    fn get_fitness(&self) -> f64;
}


