extern crate rand;

use rand::Rng;
use std::fmt::Debug;

use individual;
use individual::Individual;

#[derive(Debug, Clone)]
pub struct IndividualCaseB<T>
    where T: Clone + Debug {

    genes1: T,
    genes2: T,
    fitness: f64,
    portion_sum: f64
}

impl Individual<(Self, Self)> for IndividualCaseB<u8> {
    fn random_init() -> Self {
        let mut rng = rand::thread_rng();
        IndividualCaseB {
            genes1: rng.gen_range(0, 255),
            genes2: rng.gen_range(0, 255),
            fitness: 0.0,
            portion_sum: 0.0
        }
    }

    fn cross_with(&self, rhs: &Self) -> (Self, Self) {
        let mut child1 = IndividualCaseB {
            genes1: 0u8,
            genes2: 0u8,
            fitness: 0.0,
            portion_sum: 0.0
        };
        let mut child2 = IndividualCaseB {
            ..child1
        };

        let mut rng = rand::thread_rng();
        let k = rng.gen_range(1, 8);

        let cross = | mut gen1, mut gen2 | {
            gen1 = gen1 >> (8 - k);
            gen1 = gen1 << (8 - k);

            gen2 = gen2 << k;
            gen2 = gen2 >> k;

            gen1 | gen2
        };

        let individual1_genes1 = self.genes1;
        let individual2_genes1 = rhs.genes1;
        let individual1_genes2 = self.genes2;
        let individual2_genes2 = rhs.genes2;

        child1.genes1 = cross(individual1_genes1, individual2_genes1);
        child1.genes2 = cross(individual1_genes2, individual2_genes2);

        let individual1_genes1 = rhs.genes1;
        let individual2_genes1 = self.genes1;
        let individual1_genes2 = rhs.genes2;
        let individual2_genes2 = self.genes2;

        child2.genes1 = cross(individual1_genes1, individual2_genes1);
        child2.genes2 = cross(individual1_genes2, individual2_genes2);

        (child1, child2)
    }

    fn calculate_fitness(&mut self) -> f64 {
        let a = unsafe { individual::A };
        let b = unsafe { individual::B };
        let x = a + (b - a) * self.genes1 as f64 / (2.0f64).powi(8);
        let y = a + (b - a) * self.genes2 as f64 / (2.0f64).powi(8);
        
        let num = (x.powi(2) + y.powi(2)).sqrt().sin().powi(2) - 0.5;
        let den = (1.0 + 0.001*(x.powi(2) + y.powi(2))).powi(2);

        self.fitness = 0.5 + num/den;
        self.fitness
    }

    fn mutate(&mut self, index: usize) {
        let k = 1 << index;
        self.genes1 = self.genes1 ^ k;
        let index = if index == 0 { index + 1 } else { index };
        let k = 1 << (8 - index);
        self.genes2 = self.genes2 ^ k;
    }

    fn get_fitness(&self) -> f64 {
        self.fitness
    }
}

impl IndividualCaseB<u8> {
    pub fn get_acc_sum(&self) -> f64 {
        self.portion_sum
    }

    pub fn set_acc_sum(&mut self, sum: f64) {
        self.portion_sum = sum;
    }

    pub fn transform_gen(&self) -> (f64, f64) {
        let a = unsafe { individual::A };
        let b = unsafe { individual::B };
        let x = a + (b - a) * self.genes1 as f64 / (2.0f64).powi(8);
        let y = a + (b - a) * self.genes2 as f64 / (2.0f64).powi(8);
        (x, y)
    }
}
