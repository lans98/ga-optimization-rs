pub trait Population {
   fn init_population(size: usize) -> Self; 
   fn sort_by_fitness(&mut self);
   fn calculate_fit_acc_sum(&mut self) -> f64;
   fn make_roulette(&mut self);
   fn cross_population(&mut self, prob_of_cross: f64);
   fn mutate_population(&mut self, prob_of_mut: f64);
}


