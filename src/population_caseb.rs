extern crate rand;

use std::fmt::Debug;
use rand::Rng;

use population::Population;
use individual::Individual;
use individual_caseb::IndividualCaseB;

#[derive(Debug)]
pub struct  PopulationCaseB<T>
    where T: Clone + Debug {
    
    population: Vec<IndividualCaseB<T>>,
    accumulated_sum: f64
}

impl Population for PopulationCaseB<u8> {
    fn init_population(size: usize) -> Self {
        if size % 2 != 0 { panic!("Population size should be odd"); }

        let mut result = PopulationCaseB {
            population: Vec::with_capacity(size),
            accumulated_sum: 0.0
        };

        for _ in 0..size {
            result.population.push(IndividualCaseB::random_init());
        }

        result
    }

    fn sort_by_fitness(&mut self) {
        for individual in &mut self.population {
            individual.calculate_fitness();
        }

        self.population.sort_by(| a, b | {
            a.get_fitness().partial_cmp(&b.get_fitness()).unwrap()
        });
    }

    fn calculate_fit_acc_sum(&mut self) -> f64 {
        let mut sum = 0.0;
        for individual in &mut self.population {
            sum += individual.get_fitness();
            individual.set_acc_sum(sum);
        }

        self.accumulated_sum = sum;
        sum
    }

    fn make_roulette(&mut self) {
        let mut rng = rand::thread_rng();
        let pob_size = self.population.len();
        let mut selected_gen = Vec::with_capacity(pob_size);

        for _ in 0..pob_size {
            let random_spin = rng.gen_range(0.0, 1.0) * self.accumulated_sum;

            if random_spin < self.population[0].get_acc_sum() {
                selected_gen.push(self.population[0].clone());
                continue;
            }

            if random_spin > self.population[pob_size - 1].get_acc_sum() {
                selected_gen.push(self.population[pob_size - 1].clone());
                continue;
            }

            for i in 0..(pob_size - 1) {
                if self.population[i].get_acc_sum() <= random_spin &&
                   self.population[i + 1].get_acc_sum() >= random_spin {
                       selected_gen.push(self.population[i].clone());
                       break;
                   }
            }
        }

        self.population = selected_gen;
        self.sort_by_fitness();
    }

    fn cross_population(&mut self, prob_of_cross: f64) {
        let mut rng = rand::thread_rng();
        let pob_size = self.population.len();
        let mut childs = Vec::new();

        let mut i  = 0;
        while i < pob_size {
            let random_prob = rng.gen_range(0.0, 1.0);
            if random_prob < prob_of_cross {
                let (child0, child1) = self.population[i].cross_with(&self.population[i + 1]);
                childs.push(child0);
                childs.push(child1);
            } else {
                childs.push(self.population[i].clone());
                childs.push(self.population[i + 1].clone());
            }

            i += 2;
        }

        self.population = childs;
    }

    fn mutate_population(&mut self, prob_of_mut: f64) {
        let mut rng = rand::thread_rng();
        let pob_size = self.population.len();

        for i in 0..pob_size {
            for j in 0..8 {
                let random_prob = rng.gen_range(0.0, 1.0);
                if random_prob < prob_of_mut {
                    self.population[i].mutate(j);
                }
            }
        }
    }
}

impl PopulationCaseB<u8> {
    pub fn get_fittest(&self) -> IndividualCaseB<u8> {
        self.population[0].clone()
    }
}
